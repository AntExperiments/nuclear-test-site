package www.host13.com;

public class Variabelen {

	public static void main(String[]args) {
		
		int a = 10;		//Zahlen werden zu Variabeln
		int b = 20;
		
		String s = "Hallo";		//Int für 
		String n = " du";
		
		char c = 'j';		//Nur einen Buchstaben(/Zeichen)
		
		double d = 3.14159;		//Wie int nur auch mit ungeraden Zahlen
				
		System.out.println(a+b);
		System.out.println(s+n);
		System.out.println(c);
		System.out.println(d);
		
	}
}
