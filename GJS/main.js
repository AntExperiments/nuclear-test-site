const Gtk = imports.gi.Gtk;

Gtk.init(null, null);

// create header
const header = new Gtk.HeaderBar();
header.set_title("GJS");
header.set_subtitle("Test application with GJS");
header.set_show_close_button(true);

// create window (with header)
const window = new Gtk.Window();
window.connect('destroy', Gtk.main_quit);
window.set_default_size(350,150);
window.set_titlebar(header);

// add elements to window
window.add(new Gtk.Button({ label: "Hello World" }));
window.show_all();

Gtk.main();