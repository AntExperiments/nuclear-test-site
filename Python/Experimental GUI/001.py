import gi
#set right version of Gtk
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#define window
window = Gtk.Window()

#allow closing of window
window.connect("delete-event", Gtk.main_quit)

#show window
window.show_all()

#loop (keeps the window open)
Gtk.main()
