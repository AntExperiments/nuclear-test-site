import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        #initiate and set title
        Gtk.Window.__init__(self, title="Button Clicker")

        #Button
        self.button = Gtk.Button(label="Click me!")
        #button_clicked
        self.button.connect("clicked", self.button_clicked)
        #add to screen
        self.add(self.button)


    def button_clicked(self, widget):
        print("Button has been clicked")
        

window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
