When eating an elephant, take one bite at a time.
When working toward the solution of a problem,it always helps if you know the answer.
Provided, of course, that you know there is a problem.
A memorandum is written not to inform the reader but to protect the writer.
Power tends to corrupt; absolute power corrupts absolutely.
Anybody can win -- unless there happens to be a second entry.
When the plane you are on is late, the plane you want to transfer tois on time.
Social innovations tend to the level of minimum tolerable well being.
Never eat at a place called Mom's.  Never play cards with a man named Doc.  And never lie down with a woman who's got more troubles than you.
It is better for civilization to be going down the drain than to be coming up it.
If computers get too powerful, we can organize them into a committee - that will do them in.
Civilization advances by extending the number of important operations which we can do without thinking of them.
Almost anything is easier to get into than out of.
I'd rather have a free bottle in front of me than a prefrontal lobotomy.
Justice always prevails ... three times out of seven.
The best simple-minded test of experience in a particular area is the ability to win money in a series of bets on future occurrences in that area.
I have yet to see any problem, however complicated, which, when you looked at it in the right way, did not become still more complicated.
No matter which direction you start, it's always against the wind coming back.
Attila the Hun came from a broken home.
When working on a project, if you put away a tool that you're certain you're finished with, you will need it instantly.
Don't force it, get a larger hammer.
Any tool, when dropped, will roll into the least accessible corner of the workshop.
On the way to the corner, any dropped tool will first strike your toes.
Those whose approval you seek the most give you the least.
What the gods get away with, the cows don't.
Any order that can be misunderstood has been misunderstood.
If it moves, salute it; if it doesn't move, pick it up; if you can't pick it up, paint it.
It's always the wrong time of the month.
No books are lost by loaning except those you particularly wanted to keep.
Trouble strikes in series of threes, but when working around the house the next job after a series of three is not the fourth job -- it is the start of a brand new series of three.
If it can be borrowed and it can be broken, you will borrow it and you will break it.
What's good politics is bad economics; what's bad politics is good economics; what's good economics is bad politics; what's bad economics is good politics.
When you are over the hill, you pick up speed.
Misery no longer loves company.  Nowadays it insists on it.
Some of it plus the rest of it is all of it.
On a beautiful day like this it's hard to believe anyone can be unhappy -- but we'll work on it.
There are two classes of people: those who divide people into two classes, and those who don't.
The more ridiculous a belief system, the higher the probability of its success.
Old age is always fifteen years older than I am.
That which has not yet been taught directly can never be taughtdirectly.
If at first you don't succeed, you will never succeed.
Government intervention in the free market always leads to a lower national standard of living.
Beware of and eschew pompous prolixity.
When you're up to your nose, keep your mouth shut.
All people are cremated equal.
The trouble with having both feet on the ground is that it brings one into unbearably close contact with the world.
Litigants are amateur self-justifiers; Lawyers are the professional justifiers of others.
If everyone dislikes it, it must be looked into.
If everyone likes it, it must be looked into.
all ignorance toboggans into know
It is much harder to find a job than to keep one.
The ratio of time involved in work to time available for work is usually about 0.6.
There are two types of people:  those who divide people into two types, and those who don't.
Anyone can do any amount of work, provided it isn't the work he is supposed to be doing at that moment.
No matter which way you ride, it's uphill and against the wind.
The conclusions of most good operations research studies are obvious.
Live within your income, even if you have to borrow to do so.
Established technology tends to persist in spite of new technology.
If you want your name spelled wrong, die.
If you think education is expensive -- try ignorance.
If you're feeling good, don't worry.  You'll get over it.
Under current practices, both expenditures and revenues rise to meet each other, no matter which one may be in excess.
Never go to a doctor whose office plants have died.
A welfare state is one that assumes responsibility for the health, happiness, and general well-being of all its citizens except the taxpayers.
In any household, junk accumulates to fill the space available for its storage.
An ounce of application is worth a ton of abstraction.
The conventional wisdom is that power is an aphrodisiac. In truth, it's exhausting.
You always find something the last place you look.
A bird in the hand is dead.
If everything seems to be coming your way, you're probably in the wrong lane.
If the converse of a statement is absurd, the original statement is an insult to the intelligence and should never have been said.
The spirit of public service will rise, and the bureaucracy will multiply itself much faster, in time of grave national concern.
It's always the partner's fault.
At some time in the life cycle of virtually every organization, its ability to succeed in spite of itself runs out.
Adding manpower to a late software project makes it later.
