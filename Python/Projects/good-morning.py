import time
import datetime
import calendar
import os
import pyowm

owm = pyowm.OWM('48761464b265b3d92cfa95cf67ef718d')
observation = owm.weather_at_id(7287371)
w = observation.get_weather()
w.get_detailed_status()

dict = w.get_temperature(unit='celsius')
maxTemp = ("{}".format(dict['temp_max']))
minTemp = ("{}".format(dict['temp_min']))

lst = ["./speech_en.sh ", "Good morning Yanik, Today is ", datetime.datetime.now().strftime("%A")," the ", datetime.datetime.now().strftime("%d"),"th of ", datetime.datetime.now().strftime("%B"),". The weather today is ",w.get_detailed_status(), " with a maximal Temperature of ", maxTemp, " and an minimal Temperature of ", minTemp, " Degrees."]
print  "".join(lst)
os.system("".join(lst))
exit()
