import os
import sys
import time
import numpy
import pickle

File = "log/Fibonacci/" + str(int(time.time()))

FoundNumbers = []
x = 0
y = 1
print 'Result will be displayed here and saved in log folder.'
Max = input("Enter max Number: ")
TimeOfBeginn = int(time.time())

while 1:
    if y < Max:
        z = x + y
        x = y
        y = z
        FoundNumbers.append(y)
    else:
        os.system('clear')
        TimeOfEnd = int(time.time())
        print "Found Fibonacci numbers: ", list(FoundNumbers)
        print "Time needed for calculation:", TimeOfEnd - TimeOfBeginn, " Seconds"
        print ""

        f = open(File,'w')
        for item in FoundNumbers:
            f.write("%s\n" % item)
        f.close()

        sys.exit()
