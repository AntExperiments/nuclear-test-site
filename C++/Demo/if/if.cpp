#include <climits>
#include <iostream>

int main(){
  int i = 7;

  if (i == 5){
    std::cout << "i ist 5" << std::endl;
  } else if (i == 6){
    std::cout << "i ist 6" << std::endl;
  } else{
    std::cout << "i ist weder 5 noch 6" << std::endl;
  }

  return 0;
}
